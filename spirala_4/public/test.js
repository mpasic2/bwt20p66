let assert = chai.assert;
describe('Testovi klase Googlemeet', function() {
        it('Bez predavanja', function() {
            var vatijabla = GoogleMeet.dajZadnjePredavanje(null);
            assert.equal(vatijabla,null);
        });

        it('Bez vjezbi', function(){
            var vatijabla = GoogleMeet.dajZadnjuVježbu(null);
            assert.equal(vatijabla,null);
        });
    
        it('Prazan string', function(){
            var vatijabla = GoogleMeet.dajZadnjuVježbu(" ");
            assert.equal(vatijabla,null);
        });

        it('Vjezbe svake druge sedmice', function(){
            let ulaz = [`<!DOCTYPE html>
                <html lang="en">
                <head>
                    <script src="googlemeet.js"></script>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Document</title>
                </head>
                <body>
                    <div class="course-content">
                        <ul class="weeks">
                            <li id="section-0"><div class=\"no-overflow\">
                                    <div class=\"no-overflow\">
                                        <a href=\"https://meet.google.com/ksd-ghtz-wer\">Link za pristup vježbi 1</a>
                                        <br>Za pristup vježbi potrebno je koristiti fakultetski mail.<br><br>
                                    </div>
                                </div>
                            </li>
                            <li id="section-2"><div class=\"no-overflow\">
                                <div class=\"no-overflow\">
                                    <a href=\"https://meet.google.com/rpr-kjlk-aoh\">Link za pristup Predavanju 1</a>
                                    <br>Za pristup predavanju potrebno je koristiti fakultetski mail.<br><br>
                                </div>
                            </div>
                        </li>
                        <li id="section-3"><div class=\"no-overflow\">
                            <div class=\"no-overflow\">
                                <a href=\"https://meet.google.com/tre-hgtw-ghg\">Link za pristup vježbi 2</a>
                                <br>Za pristup vježbi potrebno je koristiti fakultetski mail.<br><br>
                            </div>
                        </div>
                    </li>
                    <li id="section-4"><div class=\"no-overflow\">
                        <div class=\"no-overflow\">
                            <a href=\"https://meet.google.com/asf-ztus-bms\">Link za pristup predavanju 2</a>
                            <br>Za pristup predavanju potrebno je koristiti fakultetski mail.<br><br>
                        </div>
                    </div>
                </li>
                        </ul>
                    </div>
                </body>
                </html>`]
                assert.equal(GoogleMeet.dajZadnjuVježbu(ulaz),'https://meet.google.com/tre-hgtw-ghg');

            
        });

        it('Neispravan url', function(){
            let ulaz = [`<!DOCTYPE html>
                <html lang="en">
                <head>
                    <script src="googlemeet.js"></script>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Document</title>
                </head>
                <body>
                    <div class="course-content">
                        <ul class="weeks">
                            <li id="section-0"><div class=\"no-overflow\">
                                    <div class=\"no-overflow\">
                                        <a href=\"https://asd-ghtz-wer\">Link za pristup vježbi 1</a>
                                        <br>Za pristup vježbi potrebno je koristiti fakultetski mail.<br><br>
                                    </div>
                                </div>
                            </li>
                            <li id="section-2"><div class=\"no-overflow\">
                                <div class=\"no-overflow\">
                                    <a href=\"https://rpr-kjlk-aoh\">Link za pristup Predavanju 1</a>
                                    <br>Za pristup predavanju potrebno je koristiti fakultetski mail.<br><br>
                                </div>
                            </div>
                        </li>
                        <li id="section-3"><div class=\"no-overflow\">
                            <div class=\"no-overflow\">
                                <a href=\"https://kpn-ydyu-aoh\">Link za pristup vježbi 2</a>
                                <br>Za pristup vježbi potrebno je koristiti fakultetski mail.<br><br>
                            </div>
                        </div>
                    </li>
                    <li id="section-4"><div class=\"no-overflow\">
                        <div class=\"no-overflow\">
                            <a href=\"https://ght-ydyu-aoh\">Link za pristup predavanju 2</a>
                            <br>Za pristup predavanju potrebno je koristiti fakultetski mail.<br><br>
                        </div>
                    </div>
                </li>
                        </ul>
                    </div>
                </body>
                </html>`]
                assert.equal(GoogleMeet.dajZadnjuVježbu(ulaz),null);
        });

        it('Bez kljucne rijeci vjezba', function(){
            let ulaz = [`<!DOCTYPE html>
                <html lang="en">
                <head>
                    <script src="googlemeet.js"></script>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Document</title>
                </head>
                <body>
                    <div class="course-content">
                        <ul class="weeks">
                            <li id="section-0"><div class=\"no-overflow\">
                                    <div class=\"no-overflow\">
                                        <a href=\"https://meet.google.com/asd-ghtz-wer\">Link za pristup  1</a>
                                        <br>Za pristup  potrebno je koristiti fakultetski mail.<br><br>
                                    </div>
                                </div>
                            </li>
                            <li id="section-2"><div class=\"no-overflow\">
                                <div class=\"no-overflow\">
                                    <a href=\"https://meet.google.com/rpr-kjlk-aoh\">Link za pristup Predavanju 1</a>
                                    <br>Za pristup predavanju potrebno je koristiti fakultetski mail.<br><br>
                                </div>
                            </div>
                        </li>
                        <li id="section-3"><div class=\"no-overflow\">
                            <div class=\"no-overflow\">
                                <a href=\"https://meet.google.com/kpn-ydyu-aoh\">Link za pristup  2</a>
                                <br>Za pristup  potrebno je koristiti fakultetski mail.<br><br>
                            </div>
                        </div>
                    </li>
                    <li id="section-4"><div class=\"no-overflow\">
                        <div class=\"no-overflow\">
                            <a href=\"https://meet.google.com/asf-ztus-bms\">Link za pristup predavanju 2</a>
                            <br>Za pristup predavanju potrebno je koristiti fakultetski mail.<br><br>
                        </div>
                    </div>
                </li>
                        </ul>
                    </div>
                </body>
                </html>`]
                assert.equal(GoogleMeet.dajZadnjuVježbu(ulaz),null);
        });


        it('Bez kljucne rijeci predavanje', function(){
            let ulaz = [`<!DOCTYPE html>
                <html lang="en">
                <head>
                    <script src="googlemeet.js"></script>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Document</title>
                </head>
                <body>
                    <div class="course-content">
                        <ul class="weeks">
                            <li id="section-0"><div class=\"no-overflow\">
                                    <div class=\"no-overflow\">
                                        <a href=\"https://meet.google.com/kpn-ydyu-aoh\">Link za pristup vjezbi 1</a>
                                        <br>Za pristup  potrebno je koristiti fakultetski mail.<br><br>
                                    </div>
                                </div>
                            </li>
                            <li id="section-2"><div class=\"no-overflow\">
                                <div class=\"no-overflow\">
                                    <a href=\"https://meet.google.com/kpn-ydyu-aoh\">Link za pristup  1</a>
                                    <br>Za pristup predavanju potrebno je koristiti fakultetski mail.<br><br>
                                </div>
                            </div>
                        </li>
                        <li id="section-3"><div class=\"no-overflow\">
                            <div class=\"no-overflow\">
                                <a href=\"https://meet.google.com/kpn-ydyu-aoh\">Link za pristup vjezbi 2</a>
                                <br>Za pristup  potrebno je koristiti fakultetski mail.<br><br>
                            </div>
                        </div>
                    </li>
                    <li id="section-4"><div class=\"no-overflow\">
                        <div class=\"no-overflow\">
                            <a href=\"https://meet.google.com/kpn-ydyu-aoh\">Link za pristup  2</a>
                            <br>Za pristup predavanju potrebno je koristiti fakultetski mail.<br><br>
                        </div>
                    </div>
                </li>
                        </ul>
                    </div>
                </body>
                </html>`]
                assert.equal(GoogleMeet.dajZadnjePredavanje(ulaz),null);
        });


        it('Van liste', function(){
            let ulaz = [`<!DOCTYPE html>
                <html lang="en">
                <head>
                    <script src="googlemeet.js"></script>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Document</title>
                </head>
                <body>
                    <div class="course-content">
                        <ul class="weeks">
                            <li id="section-0"><div class=\"no-overflow\">
                                    <div class=\"no-overflow\">
                                        <a href=\"https://meet.google.com/asd-ghtz-wer\">Link za pristup vježbi 1</a>
                                        <br>Za pristup vježbi potrebno je koristiti fakultetski mail.<br><br>
                                    </div>
                                </div>
                            </li>
                            <li id="section-2"><div class=\"no-overflow\">
                                <div class=\"no-overflow\">
                                    <a href=\"https://meet.google.com/rpr-kjlk-aoh\">Link za pristup Predavanju 1</a>
                                    <br>Za pristup predavanju potrebno je koristiti fakultetski mail.<br><br>
                                </div>
                            </div>
                        </li>
                        <li id="section-3"><div class=\"no-overflow\">
                            <div class=\"no-overflow\">
                                <a href=\"https://meet.google.com/kpn-ydyu-aoh\">Link za pristup vježbi 2</a>
                                <br>Za pristup vježbi potrebno je koristiti fakultetski mail.<br><br>
                            </div>
                        </div>
                    </li>
                    <li id="section-4"><div class=\"no-overflow\">
                        <div class=\"no-overflow\">
                            <a href=\"https://meet.google.com/asf-ztus-bms\">Link za pristup predavanju 2</a>
                            <br>Za pristup predavanju potrebno je koristiti fakultetski mail.<br><br>
                        </div>
                    </div>
                </li>
                        </ul>
                    </div>
                    <a href=\"https://meet.google.com/rwe-ydyu-aoh\">Link za pristup vježbi 1</a>
                </body>
                </html>`]
                assert.equal(GoogleMeet.dajZadnjuVježbu(ulaz),'https://meet.google.com/kpn-ydyu-aoh');
        });


        it('Predavanje u prvoj sedmici', function(){
            let ulaz = [`<!DOCTYPE html>
                <html lang="en">
                <head>
                    <script src="googlemeet.js"></script>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Document</title>
                </head>
                <body>
                    <div class="course-content">
                        <ul class="weeks">
                            <li id="section-1"><div class=\"no-overflow\">
                                    <div class=\"no-overflow\">
                                        <a href=\"https://meet.google.com/asd-ghtz-wer\">Link za pristup predavanju 1</a>
                                        <br>Za pristup predavanju potrebno je koristiti fakultetski mail.<br><br>
                                    </div>
                                </div>
                            </li>
                            <li id="section-2"><div class=\"no-overflow\">
                                <div class=\"no-overflow\">
                                    <a href=\"https://meet.google.com/rpr-kjlk-hjh\">Link za pristup vjezbi 1</a>
                                    <br>Za pristup vježbi potrebno je koristiti fakultetski mail.<br><br>
                                </div>
                            </div>
                        </li>
                        <li id="section-3"><div class=\"no-overflow\">
                            <div class=\"no-overflow\">
                                <a href=\"https://meet.google.com/kpn-ydyu-aoh\">Link za pristup vježbi 2</a>
                                <br>Za pristup vježbi potrebno je koristiti fakultetski mail.<br><br>
                            </div>
                        </div>
                    </li>
                    <li id="section-4"><div class=\"no-overflow\">
                        <div class=\"no-overflow\">
                            <a href=\"https://meet.google.com/asf-ztus-bms\">Link za pristup vjezbi 3</a>
                            <br>Za pristup vježbi potrebno je koristiti fakultetski mail.<br><br>
                        </div>
                    </div>
                </li>
                        </ul>
                    </div>
                </body>
                </html>`]
                assert.equal(GoogleMeet.dajZadnjePredavanje(ulaz),'https://meet.google.com/asd-ghtz-wer');
        });


        it('Vjezbe u prvoj sedmici', function(){
            let ulaz = [`<!DOCTYPE html>
                <html lang="en">
                <head>
                    <script src="googlemeet.js"></script>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Document</title>
                </head>
                <body>
                    <div class="course-content">
                        <ul class="weeks">
                            <li id="section-1"><div class=\"no-overflow\">
                                    <div class=\"no-overflow\">
                                        <a href=\"https://meet.google.com/asd-ghtz-wer\">Link za pristup vježbi 1</a>
                                        <br>Za pristup vježbi potrebno je koristiti fakultetski mail.<br><br>
                                    </div>
                                </div>
                            </li>
                            <li id="section-2"><div class=\"no-overflow\">
                                <div class=\"no-overflow\">
                                    <a href=\"https://meet.google.com/rpr-kjlk-hjh\">Link za pristup predavanju 1</a>
                                    <br>Za pristup predavanju potrebno je koristiti fakultetski mail.<br><br>
                                </div>
                            </div>
                        </li>
                        <li id="section-3"><div class=\"no-overflow\">
                            <div class=\"no-overflow\">
                                <a href=\"https://meet.google.com/kpn-ydyu-aoh\">Link za pristup predavanju 2</a>
                                <br>Za pristup predavanju potrebno je koristiti fakultetski mail.<br><br>
                            </div>
                        </div>
                    </li>
                    <li id="section-4"><div class=\"no-overflow\">
                        <div class=\"no-overflow\">
                            <a href=\"https://meet.google.com/asf-ztus-bms\">Link za pristup predavanju 3</a>
                            <br>Za pristup predavanju potrebno je koristiti fakultetski mail.<br><br>
                        </div>
                    </div>
                </li>
                        </ul>
                    </div>
                </body>
                </html>`]
                assert.equal(GoogleMeet.dajZadnjuVježbu(ulaz),'https://meet.google.com/asd-ghtz-wer');
        });


        it('Nevalidan html', function(){
            let ulaz = [`<!DOCTYPE html>
                <html lang="en">
                <head>
                    <script src="googlemeet.js"></script>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Document</title>
                </head>
                <body>
                    <div>
                        <ul >
                            <li><div class=\"no-overflow\">
                                    <div class=\"no-overflow\">
                                        <a href=\"https://meet.google.com/asd-ghtz-wer\">Link za pristup vježbi 1</a>
                                        <br>Za pristup vježbi potrebno je koristiti fakultetski mail.<br><br>
                                    </div>
                                </div>
                            </li>
                            <li><div class=\"no-overflow\">
                                <div class=\"no-overflow\">
                                    <a href=\"https://meet.google.com/rpr-kjlk-hjh\">Link za pristup predavanju 1</a>
                                    <br>Za pristup predavanju potrebno je koristiti fakultetski mail.<br><br>
                                </div>
                            </div>
                        </li>
                        <li><div class=\"no-overflow\">
                            <div class=\"no-overflow\">
                                <a href=\"https://meet.google.com/kpn-ydyu-aoh\">Link za pristup predavanju 2</a>
                                <br>Za pristup predavanju potrebno je koristiti fakultetski mail.<br><br>
                            </div>
                        </div>
                    </li>
                    <li><div class=\"no-overflow\">
                        <div class=\"no-overflow\">
                            <a href=\"https://meet.google.com/asf-ztus-bms\">Link za pristup predavanju 3</a>
                            <br>Za pristup predavanju potrebno je koristiti fakultetski mail.<br><br>
                        </div>
                    </div>
                </li>
                        </ul>
                    </div>
                </body>
                </html>`]
                assert.equal(GoogleMeet.dajZadnjuVježbu(ulaz),null);
        });

});