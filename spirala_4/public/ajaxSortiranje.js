function ucitajSortirano(dan,atribut,ispisiRezultat){
    const ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function(){
        if(ajax.readyState == 4 && ajax.status==200){
            var json = JSON.parse(ajax.responseText);
            ispisiRezultat(json,null);
        }
        else if(ajax.readyState==4)
            ispisiRezultat(json,"Postoji error");
    }
    var sortiranje = "";
    if(dan !=null && dan!= " " && dan!=undefined){
        sortiranje+="dan=";
        sortiranje+=dan;
    }
    if(dan !=null && dan!= " "  && dan!=undefined && atribut !=null && atribut!= " " && atribut!=undefined){
        sortiranje+="&";
    }
    if(atribut !=null && atribut!=" " && atribut!=undefined){
        sortiranje+="sort=";
        sortiranje+=atribut;
    }

    var putanja = "http://localhost:8080/raspored?"+sortiranje;
    ajax.open("GET",putanja,true);
    ajax.send();
    
}