class Raspored{
    constructor(ulazniRaspored){
        this.izlaz = [];
        var info;
        var listaPredmeta = ulazniRaspored.split("\n");
       
//greska u konstruktoru tj petlja i ide do <=
        for(var i=0;i<=listaPredmeta.length;i++){
            info = listaPredmeta[i].split(",");
            let output = {naziv : info[0], aktivnost : info[1], dan : info[2], start : info[3], end : info[4]};

            this.izlaz.push(output);
        }
        //console.log(this.izlaz);
    }

     dajTrenutnuAktivnost(ulaz,grupa){
         var aktivnosti = [];
         var tajming;
         var str = 'Trenutno nema aktivnosti';
         var dioImena = [];

        var popola = ulaz.split("T");
        var vrijeme = popola[0];
        var satiminutesekunde = popola[1];

        var pomocna =  satiminutesekunde.split(":");
        var hours = pomocna[0]+":"+pomocna[1];
        var s=pomocna[0];
        var m=pomocna[1];
        
        //console.log(s*60+m);
        

        var dani = ['ponedjeljak', 'utorak', 'srijeda', 'cetvrtak', 'petak', 'subota', 'nedjelja'];
        
        var d = new Date(vrijeme);
        var dan = d.getDay();
        var nazivDana = dani[dan-1];
        //console.log(nazivDana);
        if(nazivDana==='subota' || nazivDana==='nedjelja') return str;
        

        for(var i=0;i<this.izlaz.length;i++){
            if(this.izlaz[i].dan === nazivDana && this.izlaz[i].start<=hours && this.izlaz[i].end>hours){
                if(this.izlaz[i].aktivnost==='vjezbe'){
                    if((this.izlaz[i].naziv.includes('grupa1') && grupa==='grupa1') || (this.izlaz[i].naziv.includes('grupa2') && grupa==='grupa2')){
                        dioImena=this.izlaz[i].naziv.split("-");
                        aktivnosti.push(dioImena[0]);
                        tajming=this.izlaz[i].end;
                        continue;
                    }
                    else continue;
                }
                tajming=this.izlaz[i].end;
                dioImena=this.izlaz[i].naziv.split("-");
                        aktivnosti.push(dioImena[0]);
            }
            
        }
        if(aktivnosti==null ||aktivnosti.length<1) return str;
        //vrijeme nastave pretvaramo u minute
        var razdjela =  tajming.split(":");
        var minute = (+razdjela[0])*60+(+razdjela[1]);
        
        //ulazno vrijeme pretvaramo u minte
        var mminute = (+pomocna[0])*60+(+pomocna[1]);
        //console.log(mminute);

        var preostaloVrijeme = minute-mminute;
        aktivnosti.push(preostaloVrijeme);
        
        var izlaz=aktivnosti[0]+" "+aktivnosti[1];
        //console.log(izlaz);
        return izlaz;

    }




    dajSljedecuAktivnost (ulaz,grupa){
        var aktivnosti = [];
        var tajming;
        var str = 'Nastava je gotova za danas';
        var dioImena = [];

       var popola = ulaz.split("T");
       var vrijeme = popola[0];
       var satiminutesekunde = popola[1];

       var pomocna =  satiminutesekunde.split(":");
       var hours = pomocna[0]+":"+pomocna[1];
       var s=pomocna[0];
       var m=pomocna[1];
       
       //console.log(s*60+m);
       

       var dani = ['ponedjeljak', 'utorak', 'srijeda', 'cetvrtak', 'petak', 'subota', 'nedjelja'];
       
       var d = new Date(vrijeme);
       var dan = d.getDay();
       var brojac = dan-1;
       var brojacPetlje=0;
       var nazivDana = dani[brojac];
       //console.log(nazivDana);
       if(nazivDana==='subota' || nazivDana==='nedjelja') return str;
       
    
        //nazivDana = dani[brojac];
       for(var i=0;i<this.izlaz.length;i++){
           if(this.izlaz[i].dan === nazivDana && this.izlaz[i].start>hours){
               if(this.izlaz[i].aktivnost==='vjezbe'){
                   if((this.izlaz[i].naziv.includes('grupa1') && grupa==='grupa1') || (this.izlaz[i].naziv.includes('grupa2') && grupa==='grupa2')){
                    dioImena=this.izlaz[i].naziv.split("-");
                    aktivnosti.push(dioImena[0]);
                       tajming=this.izlaz[i].start;
                       break;
                   }
                   else continue;
               }
                tajming=this.izlaz[i].start;
                dioImena=this.izlaz[i].naziv.split("-");
                aktivnosti.push(dioImena[0]);
                break;
           }
           
           
       }
       if(aktivnosti==null || aktivnosti.length<1) return str;
       
       
    
       //console.log(tajming);
       //vrijeme nastave pretvaramo u minute
       var razdjela =  tajming.split(":");
       var minute = (+razdjela[0])*60+(+razdjela[1]);
       
       //ulazno vrijeme pretvaramo u minte
       var mminute = (+pomocna[0])*60+(+pomocna[1]);
       //console.log(mminute);

       var preostaloVrijeme = minute-mminute;
       aktivnosti.push(preostaloVrijeme);
       
       var izlaz=aktivnosti[0]+" "+aktivnosti[1];
       //console.log(izlaz);
       return izlaz;

   }

   dajPrethodnuAktivnost (ulaz,grupa){
    var aktivnosti = [];
    var tajming = '';
    var str = 'Trenutno nema aktivnosti';

   var popola = ulaz.split("T");
   var vrijeme = popola[0];
   var satiminutesekunde = popola[1];
    //ulazno vrijeme
   var pomocna =  satiminutesekunde.split(":");
   var hours = pomocna[0]+":"+pomocna[1];
   var s=pomocna[0];
   var m=pomocna[1];
   var tacnost=1;
   
   //console.log(s*60+m);
   

   var dani = ['ponedjeljak', 'utorak', 'srijeda', 'cetvrtak', 'petak', 'subota', 'nedjelja'];
   
   var d = new Date(vrijeme);
   var dan = d.getDay();
   var nazivDana = dani[dan-1];
   var pomocnaPOMOCNA = dan;
   //console.log(nazivDana);
   if(nazivDana==='subota' || nazivDana==='nedjelja') return str;
   
while(tacnost==1){
    nazivDana=dani[dan-1];
    dan--;
    if(tacnost==0)break;
   for(var i=0;i<this.izlaz.length;i++){
       if(this.izlaz[i].dan === nazivDana && this.izlaz[i].end<hours){
           if(this.izlaz[i].aktivnost==='vjezbe'){
               if((this.izlaz[i].naziv.includes('grupa1') && grupa==='grupa1') ||(this.izlaz[i].naziv.includes('grupa2') && grupa==='grupa2')){
                   aktivnosti.push(this.izlaz[i].naziv);
                   tajming=this.izlaz[i].end;
                   tacnost=0;
                   break;
               }
               else continue;
               
           }
           if(tacnost==0)break;
           tajming=this.izlaz[i].end;
           aktivnosti.push(this.izlaz[i].naziv);
           tacnost=0;   
           break;
       }
       if(tacnost==0)break;
       
   }
   if(tacnost==0)break;}


   
   var izlaz=aktivnosti[0];
   //console.log(izlaz);
   return izlaz;

}



}

