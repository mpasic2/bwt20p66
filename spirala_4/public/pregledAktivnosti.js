/*function procitajCSV(){
    var pretvoreniJSON = [];
    //console.log("Ovo radi");
    const putanja = './raspored.csv';
    if(fs.existsSync(putanja)==false) return;
    var podaci = fs.readFileSync("raspored.csv");
    var niz = podaci.toString();
    var ulaz = niz.split("\n");

    for(let i=0;i<ulaz.length;i++){
        var info;
        info = ulaz[i].split(",");
        let output = {naziv : info[0], aktivnost : info[1], dan : info[2], start : info[3], end : info[4]};
        
        pretvoreniJSON.push(output);
        }
    
    return pretvoreniJSON;
}*/
function funkcija_brojac() {

    if( typeof funkcija_brojac.counter == 'undefined' ) {
        funkcija_brojac.counter = 0;
    }
    funkcija_brojac.counter++;
    return funkcija_brojac.counter;
}

var aORd="";
var id;

window.onload = function(){
    ucitajSortirano(null,null,nacrtajTabelu);}

function nacrtajTabelu(podaci,error){
    if(error==null || error==undefined){
        
        var tabela = '<table><tr id="naslovi"><td id="1">NAZIV</td><td id="2">AKTIVNOST</td><td id="3">DAN</td><td id="4">POČETAK</td><td id="5">KRAJ</td></tr>';
        for(var i=0;i<podaci.length;i++){
            tabela+="<tr><td>" + podaci[i].naziv + "</td><td>" + podaci[i].aktivnost + "</td><td>" + podaci[i].dan + "</td><td>" + podaci[i].start + "</td><td>" +podaci[i].end +"</tr>"  ;
        }
        
        tabela+="</table>";
        document.getElementById("tabela").innerHTML=tabela;

        document.getElementById("naslovi").addEventListener('click', (slusaj)=>{
            
            var naslov=slusaj.target.innerHTML;

            naslov=naslov.toString().toLowerCase();
            if(naslov.includes("dan")) naslov="dan";
            if(naslov.includes("č")) naslov="pocetak";
            aORd="";
            

            if(funkcija_brojac()%2==0){
                aORd+='A';
                //alert(naslov);
                if(naslov.includes("naziv")) id=1;
                else if(naslov.includes("aktivnost")) id=2;
                else if(naslov.includes("dan")) id=3;
                else if(naslov.includes("pocetak")) id=4;
                else id=5;
                
                
            }
            else{
                aORd+='D';
                //alert(naslov);
                if(naslov.includes("naziv")) id=1;
                else if(naslov.includes("aktivnost")) id=2;
                else if(naslov.includes("dan")) id=3;
                else if(naslov.includes("pocetak")) id=4;
                else id=5;


                
            }
            
            ucitajSortirano(null,aORd+naslov,nacrtajTabelu);
            
        });
        if(aORd==='A'){
            document.getElementById(id).innerHTML+=" &#8595;";
            
        }

        else if(aORd==='D'){
            document.getElementById(id).innerHTML+=" &#8593;";
            
        }
               
        
    }
}