var putanjaPredmet = "http://localhost:8080/listaPredmeta";
var putanjaAktivnost = "http://localhost:8080/raspored"
window.onload=ocitavanje();
function ocitavanje(){
    var ajaxPredmeti = new XMLHttpRequest();
    var ajaxAkivnosti = new XMLHttpRequest();
    ajaxPredmeti.onreadystatechange = function(){
        if(ajaxPredmeti.readyState == 4 && ajaxPredmeti.status==200){
            var json = JSON.parse(ajaxPredmeti.responseText);
            var tabela = '<table><tr><td>REDNI BROJ</td><td >NAZIV</td></tr>';
            for(var i=1;i<=json.length;i++){
                tabela+="<tr><td>" + +i + "</td><td>" + json[i-1].naziv + "</td></tr>";
            }
        
             tabela+="</table>";
            document.getElementById("Predmeti").innerHTML=tabela;

        }
        else if(ajaxPredmeti.readyState==4){}
} 
ajaxAkivnosti.onreadystatechange = function(){
        if(ajaxAkivnosti.readyState == 4 && ajaxAkivnosti.status==200){
            var podaci = JSON.parse(ajaxAkivnosti.responseText);
            var tabela = '<table><tr><td>NAZIV</td><td>AKTIVNOST</td><td>DAN</td><td>POČETAK</td><td>KRAJ</td></tr>';
            for(var i=0;i<podaci.length;i++){
                tabela+="<tr><td>" + podaci[i].naziv + "</td><td>" + podaci[i].aktivnost + "</td><td>" + podaci[i].dan + "</td><td>" + podaci[i].start + "</td><td>" +podaci[i].end +"</tr>"  ;
            }
        
            tabela+="</table>";
            document.getElementById("Aktivnosti").innerHTML=tabela;
        }
        else if(ajaxAkivnosti.readyState==4){}
    }
    ajaxPredmeti.open("GET",putanjaPredmet,true);
    ajaxAkivnosti.open("GET",putanjaAktivnost,true);
    ajaxAkivnosti.send();
    ajaxPredmeti.send();
}
function dodavanjeURaspored(){
    var naziv = {ime:document.forms["myForm"]["naziv"].value.split("-")[0]};
    var predmet = {naziv:document.forms["myForm"]["naziv"].value,aktivnost: document.forms["myForm"]["aktivnost"].value,dan:document.forms["myForm"]["dan"].value,start:document.forms["myForm"]["start"].value,end:document.forms["myForm"]["end"].value}
    $.ajax({
        type: "POST",
        url: putanjaPredmet,
        data: naziv,
        success: (data) => {
            var postojanjePrijePost;
            if(data === "Predmet vec postoji u datoteci") postojanjePrijePost = 1;
            else postojanjePrijePost = 0;
            $.ajax({
                type: "POST",
                url: putanjaAktivnost,
                data: predmet,
                success: (data1) => {
                    document.getElementById("poruka").innerHTML = data1;
                    ocitavanje();
                    if(!(data1=== "Aktivnost je uspjesno dodana.") && postojanjePrijePost==0){
                        $.ajax({
                            type: "DELETE",
                            url: putanjaPredmet,
                            data:naziv,
                            success: (data3) => ocitavanje(),
                            error: (err3) => console.log(err),
                          });
                        }
            },
            error: (err2) => console.log(err2),      
                 
                    
                });
        },
        error: (err) => console.log(err),
    });
}