class GoogleMeet{

    static dajZadnjePredavanje(htmlInput){
        const domparser = new DOMParser();
        var doc = domparser.parseFromString(htmlInput,'text/html');
        var a;
        var br=0;
        var li;
        var meet = [];
        
        //dohvatimo sve sto se nalazi u divu s klasom course-content
        var content = doc.getElementsByClassName('course-content');

        if(content.length>0){
            //dohvatimo prvu listu koju nadjemo
            var a = content[0].getElementsByTagName("ul").item(0);
            //dohvatimo sve li unutar ul
            li = a.getElementsByTagName("li");
            

            if(li.length>0){
            //prolazimo pretljom kroz njih i trazimo a tagove tj linkove
            for(var i=0;i<li.length;i++){
                //dohvatimo sve tagove a tacnije linkove
                var linkovi = li[i].getElementsByTagName("a");
                for(var j=0;j<linkovi.length;j++){
                    //provjeravamo da li ima ista u linkovima
                    if(linkovi[j].href != null && linkovi[j].textContent != null){
                        //pretrazujemo linkove te trazimo meet.google.com
                        if(linkovi[j].href.includes("meet.google.com")){
                            //pretrazujemo linkove te trazimo kljucnu rijec predavanj
                            if(linkovi[j].textContent.includes("predavanj")){
                                //nakon sto smo nasli sve linkove od predavanja smijestamo ih u niz
                                meet.push(linkovi[j].href);
                                br++;
                                break;
                            }
                            
                        }
                    }
                }
            }

        }
        else
            return null;
                        
      
        }
        var duzina = meet.length;
        
        if(duzina<1)
            return null;
            

        
        return(meet[duzina]);

        
                
                
    }




    static dajZadnjuVježbu(htmlInput){
        const domparser = new DOMParser();
        var doc = domparser.parseFromString(htmlInput,'text/html');
        var a;
        var br=0;
        var li;
        var meet = [];

        //dohvatimo sve sto se nalazi u divu s klasom course-content
        var content = doc.getElementsByClassName('course-content');

        if(content.length>0){
            //dohvatimo prvu listu koju nadjemo
            var a = content[0].getElementsByTagName("ul").item(0);
            //dohvatimo sve li unutar ul
            li = a.getElementsByTagName("li");

            //prolazimo pretljom kroz njih i trazimo a tagove tj linkove
            for(var i=0;i<li.length;i++){
                //dohvatimo sve tagove a tacnije linkove
                var linkovi = li[i].getElementsByTagName("a");
                for(var j=0;j<linkovi.length;j++){
                    //provjeravamo da li ima ista u linkovima
                    if(linkovi[j].href != null && linkovi[j].textContent != null)
                        //pretrazujemo linkove trazeci meet.google.com
                        if(linkovi[j].href.includes("meet.google.com")){
                            //pretrazujemo linkove trazeci kljucnu rijec predavanj
                            if(linkovi[j].textContent.includes("vjezb") || linkovi[j].textContent.includes("vježb")){
                                //nakon sto smo nasli sve linkove od predavanja smijestamo ih u niz
                                meet.push(linkovi[j].href);
                                br++;
                                break;
                            }
                            
                        }
                }
            }


                        
                        
        }
        var duzina = meet.length;
        if(meet[duzina-1]==undefined || meet==null || duzina<1 || meet[duzina-1]==null) return null;
        return(meet[duzina]);

        
                
                
    }
        

        
        
}
