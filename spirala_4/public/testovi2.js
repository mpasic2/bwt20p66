let assert = chai.assert;
describe('Testovi klase Raspored', function () {
    describe("#dajTrenutnuAktivnost",function(){
    it('Kada nema aktivnosti', function () {
        let rasporedd = `BWT-grupa2,vjezbe,ponedjeljak,13:30,15:00
BWT,predavanje,ponedjeljak,15:00,18:00
MUR1,predavanje,utorak,09:00,11:00
MUR1-grupa1,vjezbe,srijeda,11:00,12:30
MUR1-grupa2,vjezbe,srijeda,12:30,14:00
RMA,predavanje,ponedjeljak,09:00,12:00
BWT-grupa1,vjezbe,ponedjeljak,12:00,13:30
FWT-grupa2,vjezbe,srijeda,11:00,12:30
FWT-grupa1,vjezbe,srijeda,12:30,14:00
ASP,predavanje,srijeda,09:00,12:00
MUR2,predavanje,cetvrtak,12:00,15:00
FWT,predavanje,cetvrtak,09:00,10:30`;
        let ras = new Raspored(rasporedd);
        var mojUlaz = "09-10-2020T12:00:00";
        var vatijabla = ras.dajTrenutnuAktivnost(mojUlaz,"");
        assert.equal(vatijabla, "Trenutno nema aktivnosti");
    });

    it('Aktivnost na pocetku', function () {
        let rasporedd = `BWT-grupa2,vjezbe,ponedjeljak,13:30,15:00
BWT,predavanje,ponedjeljak,15:00,18:00
MUR1,predavanje,utorak,09:00,11:00
MUR1-grupa1,vjezbe,srijeda,11:00,12:30
MUR1-grupa2,vjezbe,srijeda,12:30,14:00
RMA,predavanje,ponedjeljak,09:00,12:00
BWT-grupa1,vjezbe,ponedjeljak,12:00,13:30
FWT-grupa2,vjezbe,srijeda,11:00,12:30
FWT-grupa1,vjezbe,srijeda,12:30,14:00
ASP,predavanje,srijeda,09:00,12:00
MUR2,predavanje,cetvrtak,12:00,15:00
FWT,predavanje,cetvrtak,09:00,10:30`;
        let ras = new Raspored(rasporedd);
        var mojUlaz = "14-10-2020T10:15:00";
        var vatijabla = ras.dajTrenutnuAktivnost(mojUlaz,"");
        assert.equal(vatijabla, "ASP 105");
    });

    it('Aktivnost na kraju', function () {
        let rasporedd = `BWT-grupa2,vjezbe,ponedjeljak,13:30,15:00
BWT,predavanje,ponedjeljak,15:00,18:00
MUR1,predavanje,utorak,09:00,11:00
MUR1-grupa1,vjezbe,srijeda,11:00,12:30
MUR1-grupa2,vjezbe,srijeda,12:30,14:00
RMA,predavanje,ponedjeljak,09:00,12:00
BWT-grupa1,vjezbe,ponedjeljak,12:00,13:30
FWT-grupa2,vjezbe,srijeda,11:00,12:30
FWT-grupa1,vjezbe,srijeda,12:30,14:00
ASP,predavanje,srijeda,09:00,12:00
MUR2,predavanje,cetvrtak,12:00,15:00
FWT,predavanje,cetvrtak,09:00,10:30`;
        let ras = new Raspored(rasporedd);
        var mojUlaz = "15-10-2020T15:00:00";
        var vatijabla = ras.dajTrenutnuAktivnost(mojUlaz,"grupa2");
        assert.equal(vatijabla, "Trenutno nema aktivnosti");
    });

    it('Aktivnost na kraju', function () {
        let rasporedd = `BWT-grupa2,vjezbe,ponedjeljak,13:30,15:00
BWT,predavanje,ponedjeljak,15:00,18:00
MUR1,predavanje,utorak,09:00,11:00
MUR1-grupa1,vjezbe,srijeda,11:00,12:30
MUR1-grupa2,vjezbe,srijeda,12:30,14:00
RMA,predavanje,ponedjeljak,09:00,12:00
BWT-grupa1,vjezbe,ponedjeljak,12:00,13:30
FWT-grupa2,vjezbe,srijeda,11:00,12:30
FWT-grupa1,vjezbe,srijeda,12:30,14:00
ASP,predavanje,srijeda,09:00,12:00
MUR2,predavanje,cetvrtak,12:00,15:00
FWT,predavanje,cetvrtak,09:00,10:30`;
        let ras = new Raspored(rasporedd);
        var mojUlaz = "15-10-2020T14:59:00";
        var vatijabla = ras.dajTrenutnuAktivnost(mojUlaz,"grupa2");
        assert.equal(vatijabla, "MUR2 1");
    });

    it('Pogresna grupa', function () {
        let rasporedd = `BWT-grupa2,vjezbe,ponedjeljak,13:30,15:00
BWT,predavanje,ponedjeljak,15:00,18:00
MUR1,predavanje,utorak,09:00,11:00
MUR1-grupa1,vjezbe,srijeda,11:00,12:30
MUR1-grupa2,vjezbe,srijeda,12:30,14:00
RMA,predavanje,ponedjeljak,09:00,12:00
BWT-grupa1,vjezbe,ponedjeljak,12:00,13:30
FWT-grupa2,vjezbe,srijeda,11:00,12:30
FWT-grupa1,vjezbe,srijeda,12:30,14:00
ASP,predavanje,srijeda,09:00,12:00
MUR2,predavanje,cetvrtak,12:00,15:00
FWT,predavanje,cetvrtak,09:00,10:30`;
        let ras = new Raspored(rasporedd);
        var mojUlaz = "12-10-2020T12:00:00";
        var vatijabla = ras.dajTrenutnuAktivnost(mojUlaz,"grupa2");
        assert.equal(vatijabla, "Trenutno nema aktivnosti");
    });


    it('Ispravna grupa', function () {
        let rasporedd = `BWT-grupa2,vjezbe,ponedjeljak,13:30,15:00
BWT,predavanje,ponedjeljak,15:00,18:00
MUR1,predavanje,utorak,09:00,11:00
MUR1-grupa1,vjezbe,srijeda,11:00,12:30
MUR1-grupa2,vjezbe,srijeda,12:30,14:00
RMA,predavanje,ponedjeljak,09:00,12:00
BWT-grupa1,vjezbe,ponedjeljak,12:00,13:30
FWT-grupa2,vjezbe,srijeda,11:00,12:30
FWT-grupa1,vjezbe,srijeda,12:30,14:00
ASP,predavanje,srijeda,09:00,12:00
MUR2,predavanje,cetvrtak,12:00,15:00
FWT,predavanje,cetvrtak,09:00,10:30`;
        let ras = new Raspored(rasporedd);
        var mojUlaz = "11-11-2020T13:01:00";
        var vatijabla = ras.dajTrenutnuAktivnost(mojUlaz,"grupa1");
        assert.equal(vatijabla, "FWT 59");
    });
});

describe("#dajSljedecuAktivnost",function(){
    it('Sljedeca vjezba pogresna grupa', function () {
        let rasporedd = `BWT-grupa2,vjezbe,ponedjeljak,13:30,15:00
BWT,predavanje,ponedjeljak,15:00,18:00
MUR1,predavanje,utorak,09:00,11:00
MUR1-grupa1,vjezbe,srijeda,11:00,12:30
MUR1-grupa2,vjezbe,srijeda,12:30,14:00
RMA,predavanje,ponedjeljak,09:00,12:00
BWT-grupa1,vjezbe,ponedjeljak,12:00,13:30
FWT-grupa2,vjezbe,srijeda,11:00,12:30
FWT-grupa1,vjezbe,srijeda,12:30,14:00
ASP,predavanje,srijeda,09:00,12:00
MUR2,predavanje,cetvrtak,12:00,15:00
FWT,predavanje,cetvrtak,09:00,10:30`;
        let ras = new Raspored(rasporedd);
        var mojUlaz = "09-11-2020T09:00:00";
        var vatijabla = ras.dajSljedecuAktivnost(mojUlaz,"grupa1");
        assert.equal(vatijabla, "BWT 360");
    });

    it('Sljedeca vjezba pogresna grupa', function () {
        let rasporedd = `BWT-grupa2,vjezbe,ponedjeljak,13:30,15:00
BWT,predavanje,ponedjeljak,15:00,18:00
MUR1,predavanje,utorak,09:00,11:00
MUR1-grupa1,vjezbe,srijeda,11:00,12:30
MUR1-grupa2,vjezbe,srijeda,12:30,14:00
RMA,predavanje,ponedjeljak,09:00,12:00
BWT-grupa1,vjezbe,ponedjeljak,12:00,13:30
FWT-grupa2,vjezbe,srijeda,11:00,12:30
FWT-grupa1,vjezbe,srijeda,12:30,14:00
ASP,predavanje,srijeda,09:00,12:00
MUR2,predavanje,cetvrtak,12:00,15:00
FWT,predavanje,cetvrtak,09:00,10:30`;
        let ras = new Raspored(rasporedd);
        var mojUlaz = "11-11-2020T13:00:00";
        var vatijabla = ras.dajSljedecuAktivnost(mojUlaz,"grupa1");
        assert.equal(vatijabla, "Nastava je gotova za danas");
    });

});

describe("#dajPrethodnuAktivnost",function(){

    it('Prethodna vjezba pogresna grupa', function () {
        let rasporedd = `BWT-grupa2,vjezbe,ponedjeljak,13:30,15:00
BWT,predavanje,ponedjeljak,15:00,18:00
MUR1,predavanje,utorak,09:00,11:00
MUR1-grupa1,vjezbe,srijeda,11:00,12:30
MUR1-grupa2,vjezbe,srijeda,12:30,14:00
RMA,predavanje,ponedjeljak,09:00,12:00
BWT-grupa1,vjezbe,ponedjeljak,12:00,13:30
FWT-grupa2,vjezbe,srijeda,11:00,12:30
FWT-grupa1,vjezbe,srijeda,12:30,14:00
ASP,predavanje,srijeda,09:00,12:00
MUR2,predavanje,cetvrtak,12:00,15:00
FWT,predavanje,cetvrtak,09:00,10:30`;
        let ras = new Raspored(rasporedd);
        var mojUlaz = "05-11-2020T11:30:00";
        var vatijabla = ras.dajPrethodnuAktivnost(mojUlaz,"grupa1");
        assert.equal(vatijabla, "FWT");
    });

    it('Vracanje sa ponedjeljka na petak', function () {
        let rasporedd = `BWT-grupa2,vjezbe,ponedjeljak,13:30,15:00
BWT,predavanje,ponedjeljak,15:00,18:00
MUR1,predavanje,utorak,09:00,11:00
MUR1-grupa1,vjezbe,srijeda,11:00,12:30
MUR1-grupa2,vjezbe,srijeda,12:30,14:00
RMA,predavanje,ponedjeljak,09:00,12:00
BWT-grupa1,vjezbe,ponedjeljak,12:00,13:30
FWT-grupa2,vjezbe,srijeda,11:00,12:30
FWT-grupa1,vjezbe,srijeda,12:30,14:00
ASP,predavanje,srijeda,09:00,12:00
MUR2,predavanje,cetvrtak,12:00,15:00
FWT,predavanje,cetvrtak,09:00,10:30
FWT,predavanje,petak,18:00,19:30`;
        let ras = new Raspored(rasporedd);
        var mojUlaz = "12-10-2020T20:00:00";
        var vatijabla = ras.dajPrethodnuAktivnost(mojUlaz,"grupa1");
        assert.equal(vatijabla, "BWT");
    });


});

});