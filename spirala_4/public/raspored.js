class Raspored{
    constructor(ulazniRaspored){
        this.izlaz = [];
        var info;
        var listaPredmeta = ulazniRaspored.split("\n");
       

        for(var i=0;i<listaPredmeta.length;i++){
            info = listaPredmeta[i].split(",");
            let output = {naziv : info[0], aktivnost : info[1], dan : info[2], start : info[3], end : info[4]};

            this.izlaz.push(output);
        }
        //console.log(this.izlaz);
    }

    // Komentarima sam objasnio samo prvu funkciju jer sve 3 funkcije rade na manje-vise isti nacin 

     dajTrenutnuAktivnost(ulaz,grupa){
         var aktivnosti = [];
         var tajming;
         var str = 'Trenutno nema aktivnosti';
         var dioImena = [];
        //ulaz dijelimo na T kako bismo dobili uneseni datum i vrijeme
        var popola = ulaz.split("T");
        var vrijeme = popola[0];
        var satiminutesekunde = popola[1];
        //dijelimo vrijeme kako bismo uzeli samo sate i minute
        var pomocna =  satiminutesekunde.split(":");
        var hours = pomocna[0]+":"+pomocna[1];
        var s=pomocna[0];
        var m=pomocna[1];
        
        
        
//trazimo naziv dana koristeci metodu getDay()
        var dani = ['ponedjeljak', 'utorak', 'srijeda', 'cetvrtak', 'petak', 'subota', 'nedjelja'];
        var vrijemeSplit = vrijeme.split("-");
        //console.log(vrijemeSplit);

        var d = new Date(vrijemeSplit[2],vrijemeSplit[1]-1,vrijemeSplit[0]);
        var dan = d.getDay();
        var nazivDana = dani[dan-1];
        //console.log(nazivDana);
        if(nazivDana==='subota' || nazivDana==='nedjelja') return str;
        
//prolazimo for petljom kroz raspored te trazimo da uneseno vrijeme bude ili na pocetku jedne aktivnosti ili na kraju
        for(var i=0;i<this.izlaz.length;i++){
            if(this.izlaz[i].dan === nazivDana && this.izlaz[i].start<=hours && this.izlaz[i].end>hours){
                //ukoliko je data aktivnost vjezba trebamo provjeriti da li je grupa uredu
                if(this.izlaz[i].aktivnost==='vjezbe'){
                    if((this.izlaz[i].naziv.includes('grupa1') && grupa==='grupa1') || (this.izlaz[i].naziv.includes('grupa2') && grupa==='grupa2')){
                        //ukoliko jeste dodajemo je u niz te biljezimo i njeno vrijeme koje ce nam kasnije trebati
                        dioImena=this.izlaz[i].naziv.split("-");
                        aktivnosti.push(dioImena[0]);
                        tajming=this.izlaz[i].end;
                        break;
                    }
                    else continue;
                }
                //ukoliko data aktivnost nije vjezba samo cemo je dodati u niz aktivnosti
                tajming=this.izlaz[i].end;
                dioImena=this.izlaz[i].naziv.split("-");
                        aktivnosti.push(dioImena[0]);
                        break;
            }
            
        }
        //provjeravamo da li postoje trenutne aktivnosti
        if(aktivnosti==null ||aktivnosti.length<1) return str;
        //vrijeme nastave pretvaramo u minute
        var razdjela =  tajming.split(":");
        var minute = (+razdjela[0])*60+(+razdjela[1]);
        
        //ulazno vrijeme pretvaramo u minte
        var mminute = (+pomocna[0])*60+(+pomocna[1]);
        
        //racunamo preostalo vrijeme
        var preostaloVrijeme = minute-mminute;
        aktivnosti.push(preostaloVrijeme);
        
        var izlaz=aktivnosti[0]+" "+aktivnosti[1];
        return izlaz;

    }




    dajSljedecuAktivnost (ulaz,grupa){
        var aktivnosti = [];
        var tajming;
        var str = 'Nastava je gotova za danas';
        var dioImena = [];

       var popola = ulaz.split("T");
       var vrijeme = popola[0];
       var satiminutesekunde = popola[1];

       var pomocna =  satiminutesekunde.split(":");
       var hours = pomocna[0]+":"+pomocna[1];
       var s=pomocna[0];
       var m=pomocna[1];
       
       //console.log(s*60+m);
       

       var dani = ['ponedjeljak', 'utorak', 'srijeda', 'cetvrtak', 'petak', 'subota', 'nedjelja'];
       var vrijemeSplit = vrijeme.split("-");
       var d = new Date(vrijemeSplit[2],vrijemeSplit[1]-1,vrijemeSplit[0]);
       
       var dan = d.getDay();
       var brojac = dan-1;
       var brojacPetlje=0;
       var nazivDana = dani[brojac];
       //console.log(nazivDana);
       if(nazivDana==='subota' || nazivDana==='nedjelja') return str;
       
    
        //nazivDana = dani[brojac];
       for(var i=0;i<this.izlaz.length;i++){
           if(this.izlaz[i].dan === nazivDana && this.izlaz[i].start>hours){
               if(this.izlaz[i].aktivnost==='vjezbe'){
                   if((this.izlaz[i].naziv.includes('grupa1') && grupa==='grupa1') || (this.izlaz[i].naziv.includes('grupa2') && grupa==='grupa2')){
                    dioImena=this.izlaz[i].naziv.split("-");
                    aktivnosti.push(dioImena[0]);
                       tajming=this.izlaz[i].start;
                       break;
                   }
                   else continue;
               }
                tajming=this.izlaz[i].start;
                dioImena=this.izlaz[i].naziv.split("-");
                aktivnosti.push(dioImena[0]);
                break;
           }
           
           
       }
       if(aktivnosti==null || aktivnosti.length<1) return str;
       
       
    
       //console.log(tajming);
       //vrijeme nastave pretvaramo u minute
       var razdjela =  tajming.split(":");
       var minute = (+razdjela[0])*60+(+razdjela[1]);
       
       //ulazno vrijeme pretvaramo u minte
       var mminute = (+pomocna[0])*60+(+pomocna[1]);
       //console.log(mminute);

       var preostaloVrijeme = minute-mminute;
       aktivnosti.push(preostaloVrijeme);
       
       var izlaz=aktivnosti[0]+" "+aktivnosti[1];
       //console.log(izlaz);
       return izlaz;

   }

   dajPrethodnuAktivnost (ulaz,grupa){
    var aktivnosti = [];
    var tajming = '';
    var str = 'Trenutno nema aktivnosti';

   var popola = ulaz.split("T");
   var vrijeme = popola[0];
   var satiminutesekunde = popola[1];
    //ulazno vrijeme
   var pomocna =  satiminutesekunde.split(":");
   var hours = pomocna[0]+":"+pomocna[1];
   var s=pomocna[0];
   var m=pomocna[1];
   var tacnost=1;
   
   //console.log(s*60+m);
   

   var dani = ['ponedjeljak', 'utorak', 'srijeda', 'cetvrtak', 'petak', 'subota', 'nedjelja'];
   
   var vrijemeSplit = vrijeme.split("-");
   var d = new Date(vrijemeSplit[2],vrijemeSplit[1]-1,vrijemeSplit[0]);
   var dan = d.getDay();
   var nazivDana = dani[dan-1];
   var pomocnaPOMOCNA = dan;
   //console.log(nazivDana);
   if(nazivDana==='subota' || nazivDana==='nedjelja') { console.log("jebo si jeza u ledja"); return str;}
   
while(tacnost==1){
    if(tacnost==0)break;
    if(dan<=0) dan=7; 
    nazivDana=dani[dan-1];
    //console.log(nazivDana);
    dan--;
    if(tacnost==0)break;
   for(var i=0;i<this.izlaz.length;i++){
       if(this.izlaz[i].dan === nazivDana && this.izlaz[i].end<=hours){
           if(this.izlaz[i].aktivnost==='vjezbe'){
               if((this.izlaz[i].naziv.includes('grupa1') && grupa==='grupa1') ||(this.izlaz[i].naziv.includes('grupa2') && grupa==='grupa2')){
                   aktivnosti.push(this.izlaz[i].naziv);
                   tajming=this.izlaz[i].end;
                   tacnost=0;
                   break;
               }
               else if(aktivnosti==null || aktivnosti==undefined || aktivnosti.length<1) continue;
               else{ tacnost=0; break;}
               
           }
           if(tacnost==0)break;
           tajming=this.izlaz[i].end;
           aktivnosti.push(this.izlaz[i].naziv);
           tacnost=0;   
           break;
       }
       if(tacnost==0)break;
       
   }
   if(tacnost==0)break;}


   
   var izlaz=aktivnosti[0];
   console.log(izlaz);
   return izlaz;

}



}
/*
let rasporedd=`BWT-grupa2,vjezbe,ponedjeljak,13:30,15:00
BWT,predavanje,ponedjeljak,15:00,18:00
MUR1,predavanje,utorak,09:00,11:00
MUR1-grupa1,vjezbe,srijeda,11:00,12:30
MUR1-grupa2,vjezbe,srijeda,12:30,14:00
RMA,predavanje,ponedjeljak,09:00,12:00
BWT-grupa1,vjezbe,ponedjeljak,12:00,13:30
FWT-grupa2,vjezbe,srijeda,11:00,12:30
FWT-grupa1,vjezbe,srijeda,12:30,14:00
ASP,predavanje,srijeda,09:00,12:00
MUR2,predavanje,cetvrtak,12:00,15:00
FWT,predavanje,cetvrtak,09:00,10:30
FWT,predavanje,petak,18:00,19:30`; 
//mjesec-dan-godina
let time = "12-10-2020T20:00:00";
//var ispis = Raspored.dajTrenutnuAktivnost(time,"");
 var ispisi = new Raspored(rasporedd);
 //rma 0
ispisi.dajTrenutnuAktivnost(time,"grupa1");
//console.log(ispisi);*/