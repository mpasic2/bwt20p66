const http = require('http');
const fs = require('fs');
const url = require('url'); 
const express = require('express');
const bodyParser = require('body-parser');
const querystring = require('querystring');
const app = express();
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const fastcsv = require('fast-csv');
const { PassThrough } = require('stream');
const sortArray = require('sort-array');

const db = require('./db/db.js');
db.sequelize.sync({force:true}).then(function(){
    console.log("Tables creted");
    
});

function procitajCSV(){
    var pretvoreniJSON = [];
    //console.log("Ovo radi");
    const putanja = './public/raspored.csv';
    if(fs.existsSync(putanja)==false) return;
    var podaci = fs.readFileSync("public/raspored.csv");
    var niz = podaci.toString();
    var ulaz = niz.split("\n");

    for(let i=0;i<ulaz.length;i++){
        var info;
        info = ulaz[i].split(",");
        let output = {naziv : info[0], aktivnost : info[1], dan : info[2], start : info[3], end : info[4]};
        
        pretvoreniJSON.push(output);
        }
    
    return pretvoreniJSON;
}
function procitajCSVPredmete(){
    var pretvoreniJSON = [];
    const putanja = './public/predmeti.csv';
    if(fs.existsSync(putanja)==false) return;
    var podaci = fs.readFileSync("public/predmeti.csv");
    var niz = podaci.toString();
    var ulaz = niz.split(",");

    for(let i=0;i<ulaz.length;i++){
        let output = {naziv : ulaz[i]};
        pretvoreniJSON.push(output);
        }
    //console.log(pretvoreniJSON);
    return pretvoreniJSON;
}



function poredi(atribut,order) {
    var sort_order = 1;
    if(order === "desc"){
        sort_order = -1;
    }
    var nizDana = ["ponedjeljak","utorak","srijeda","cetvrtak","petak","subota","nedjelja"]
    return function (a, b){
        if(atribut==="dan"){
            if(nizDana.findIndex(e=>a[atribut] == e)<nizDana.findIndex(e=>b[atribut] == e)){
                return -1 * sort_order;
            }else if(nizDana.findIndex(e=>a[atribut] == e)>nizDana.findIndex(e=>b[atribut] == e)){
                return 1 * sort_order;
            }else{
                return 0 * sort_order;
        }
        }else{
        if(a[atribut] < b[atribut]){
                return -1 * sort_order;
        }else if(a[atribut] > b[atribut]){
                return 1 * sort_order;
        }else{
                return 0 * sort_order;
        }
    }
}
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(express.static(__dirname + '/public'));
app.post('/raspored',function(req,res){
    res.setHeader('Content-Type', 'text/plain');
    var datoteka = procitajCSV();
                const podaci = req.body;
                //console.log(podaci);
                var tacnost = 0;
                var validnostGrupe = 0;
                if(podaci.naziv!='' && (podaci.aktivnost==='predavanje' || podaci.aktivnost==='vjezbe') &&
                    (podaci.dan==='ponedjeljak' || podaci.dan==='utorak' || podaci.dan==='srijeda' || podaci.dan==='cetvrtak' || podaci.dan==='petak') ){

                            for(var i=0;i<datoteka.length;i++){
                                
                                if(datoteka[i].dan===podaci.dan){
                                
                                    if( podaci.start>=datoteka[i].start && podaci.start<datoteka[i].end ) {
                                        

                                        if(podaci.aktivnost==='vjezbe' && datoteka[i].aktivnost==='vjezbe'){

                                            var nizGrupaIzDatoteke = datoteka[i].naziv.split("-");
                                            var grupaIzDatoteke = nizGrupaIzDatoteke[1];

                                            var nizGrupe = podaci.naziv.split("-");
                                            var grupa = nizGrupe[1];

                                            if(grupaIzDatoteke===grupa){console.log(datoteka[i].naziv); tacnost=1; break;}
                                            else tacnost=0;
                                        }
                                        else{
                                        tacnost = 1;
                                        break;}
                                    }
                                }
                            }
                        
                        
                        if(tacnost==0){
                            //zapisujemo u datoteku
                            
                            let izlaz = "\n" + podaci.naziv + ',' + podaci.aktivnost + ',' + podaci.dan + ',' + podaci.start + ',' + podaci.end;
                            fs.appendFile('public/raspored.csv',izlaz,(error)=>{
                                if(error) throw error;
                            });

                            res.end("Aktivnost je uspjesno dodana.");


                            
                        }
                        else res.end("Vec imaju aktivnosti u ovom terminu. Pokusajte neki drugi!");
                       

                    }
                
            });            


app.get('/raspored',function(req,res){
    var datoteka = procitajCSV();
    var niz = "";
    if(req.url.includes("sort")){
        var path = req.url.split("sort=")[1];
        var sort = path[0];
        if(path.includes("naziv")) var atribut="naziv";
        else if(path.includes("aktivnost")) var atribut="aktivnost";
        else if(path.includes("dan")) var atribut="dan";
        else if(path.includes("pocetak")) var atribut="start";
        else if(path.includes("kraj")) var atribut="end";

        
        if(sort==="A")
            sort="asc";

        else
            sort="desc";

        
        datoteka.sort(poredi(atribut,sort));
        
    }
    
    
    if(req.url.includes("ponedjeljak") || req.url.includes("utorak") || req.url.includes("srijeda") 
    || req.url.includes("cetvrtak") || req.url.includes("petak") ){
        var dan = "";
            if(req.url.includes("ponedjeljak")) dan="ponedjeljak";
            else if(req.url.includes("utorak")) dan="utorak";
            else if(req.url.includes("srijeda")) dan="srijeda";
            else if(req.url.includes("cetvrtak")) dan="cetvrtak";
            else if(req.url.includes("petak")) dan="petak";
            

            if(req.headers.accept==="text/csv"){
                
                const podaci = req.body;
                
                
                
                    for(i=0;i<datoteka.length;i++){
                        if(datoteka[i].dan===dan){
                            niz+=datoteka[i].naziv+", "+datoteka[i].aktivnost+", "+datoteka[i].dan+", "+datoteka[i].start+", "+datoteka[i].end;                           
                            niz+="\n";
                        }
                    }
                    console.log(niz);
                    res.end(niz);
                    

                
            }
            else{
                
                var varijabla="";
                req.on('data', (bodyParts)=>{varijabla+=bodyParts;});
                req.on('end',() => {
                const podaci = querystring.parse(varijabla);
                
                    for(i=0;i<datoteka.length;i++){
                        if(datoteka[i].dan===dan){
                            res.write(JSON.stringify(datoteka[i]));
                            res.write("\n");
                        }
                    }
                    res.end(niz);
                    

                });
            }
    }
    else{
        const putanja = './public/raspored.csv';
        
        try{
            if(fs.existsSync(putanja)){
                if(req.headers.accept==="text/csv"){
                    res.setHeader('Content-Type', 'text/csv');
                    var ispis = fs.readFileSync("public/raspored.csv");
                    res.end(ispis);
                }
                else{
                    res.setHeader('Content-Type', 'application/json');
                    res.end(JSON.stringify(datoteka));
                }
            }
            else {
                res.setHeader('Content-Type', 'application/json');
                res.end(JSON.stringify({greska: "Datoteka raspored.csv nije kreirana!"}));
            }
        }
        catch(error){
            res.end(JSON.stringify({greska: "Datoteka raspored.csv nije kreirana!"}));
        }
    }

    
});
app.delete('/listaPredmeta',function(req,res){
    var naziv = req.body.ime;
    var predmetiJson = procitajCSVPredmete();
    var ukonjeni = predmetiJson.findIndex(a=> a.naziv.toLowerCase() === naziv.toLowerCase());
    var vracanje ="";
    if(ukonjeni >-1){
        predmetiJson.splice(ukonjeni,1);
        for(var i of predmetiJson){
            vracanje+=i.naziv+",";
        }
        fs.writeFileSync("public/predmeti.csv",vracanje.substring(0,vracanje.length-1));
        res.end("Predmet uspjesno obrisan");
        return;
    }
    res.end("Predmet ne postoji u datoteci")

});
app.get('/listaPredmeta',function(req,res){
    res.end(JSON.stringify(procitajCSVPredmete()));
});
app.post('/listaPredmeta',function(req,res){
    var JSONPredmeti = procitajCSVPredmete();
    var naziv = req.body.ime;
    var potrebni = JSONPredmeti.findIndex(a=> a.naziv.toLowerCase() === naziv.toLowerCase());
    if(potrebni == -1){
        JSONPredmeti.push({naziv:naziv});
        var noviDoc = ""
        for(var i of JSONPredmeti){
            noviDoc+=i.naziv+",";
        }
        fs.writeFileSync("public/predmeti.csv",noviDoc.substring(0,noviDoc.length-1));
        res.end("Predmet uspjesno dodan");
    }else{
        res.end("Predmet vec postoji u datoteci");
    }
});

//za predmet
app.post('/v2/predmet',function(req,res){
    var nazivPredemta = req.body.naziv;
    db.predmet.create({naziv:nazivPredemta}).then(
        res.end("Dodan predmet"));
});

app.get('/v2/predmet',function(req,res){
    res.setHeader('Content-Type', 'application/json');
    db.predmet.findAll({where:{naziv:req.body.naziv}}).then(function(rez){
        res.end(JSON.stringify(rez));
    });
});

app.delete('/v2/predmet',function(req,res){
    db.predmet.destroy({where:{naziv:req.body.naziv}}).then(
        res.end("Predmet obrisan"));
});

app.put('/v2/predmet',function(req,res){
    var url = req.url;
    var ide = url.split("id=");

    db.predmet.update({naziv:req.body.naziv},{where:{id:ide[1]}}).then(
        res.end("Azuriran predmet"));
});


//za grupu
app.post('/v2/grupa',function(req,res){
    var broj =parseInt(req.body.predmet);
    db.grupa.create({naziv:req.body.naziv,predmet_id:broj}).then((id)=>{
        res.end("Dodana grupa"+id);
    });
        
});

app.get('/v2/grupa',function(req,res){
    res.setHeader('Content-Type', 'application/json');
    db.grupa.findAll({where:{naziv:req.body.naziv}}).then(function(rez){
        res.end(JSON.stringify(rez));
    });
});

app.delete('/v2/grupa',function(req,res){
    db.grupa.destroy({where:{naziv:req.body.naziv}}).then(
        res.end("Grupa je obrisana"));
});

app.put('/v2/grupa',function(req,res){
    var url = req.url;
    var ide = url.split("id=");

    db.grupa.update({naziv:req.body.naziv},{where:{id:ide[1]}}).then(
        res.end("Azurirana grupa"));
});



//za aktivnost
app.post('/v2/aktivnost',function(req,res){
    db.aktivnost.create({naziv:req.body.naziv , pocetak:req.body.pocetak , kraj:req.body.kraj , predmet_id:req.body.predmet_id , grupa_id:req.body.grupa_id , dan_id:req.body.dan_id , tip_id:req.body.tip_id}).then(
        res.end("Dodana aktivnost"));
});

app.get('/v2/aktivnost',function(req,res){
    res.setHeader('Content-Type', 'application/json');
    db.aktivnost.findAll({where:{naziv:req.body.naziv}}).then(function(rez){
        res.end(JSON.stringify(rez));
    });
});

app.delete('/v2/aktivnost',function(req,res){
    db.aktivnost.destroy({where:{naziv:req.body.naziv}}).then(
        res.end("Aktivnost obrisana"));
});

app.put('/v2/aktivnost',function(req,res){
    var url = req.url;
    var ide = url.split("id=");

    db.aktivnost.update({naziv:req.body.naziv,pocetak:req.body.pocetak,kraj:req.body.kraj},{where:{id:ide[1]}}).then(
        res.end("Azurirana aktivnost"));
});



//za dan
app.post('/v2/dan',function(req,res){
    db.dan.create({naziv:req.body.naziv}).then(
        res.end("Dodan dan"));
});

app.get('/v2/dan',function(req,res){
    res.setHeader('Content-Type', 'application/json');
    db.dan.findAll({where:{naziv:req.body.naziv}}).then(function(rez){
        res.end(JSON.stringify(rez));
    });
});

app.delete('/v2/dan',function(req,res){
    db.dan.destroy({where:{naziv:req.body.naziv}}).then(
        res.end("Predmet obrisan"));
});

app.put('/v2/dan',function(req,res){
    var url = req.url;
    var ide = url.split("id=");

    db.grupa.update({naziv:req.body.naziv},{where:{id:ide[1]}}).then(
        res.end("Azurirana grupa"));
});


//za tip
app.post('/v2/tip',function(req,res){
    db.tip.create({naziv:req.body.naziv}).then(
        res.end("Dodan tip"));
});

app.get('/v2/tip',function(req,res){
    res.setHeader('Content-Type', 'application/json');
    db.tip.findAll({where:{naziv:req.body.naziv}}).then(function(rez){
        res.end(JSON.stringify(rez));
    });
});

app.delete('/v2/tip',function(req,res){
    db.tip.destroy({where:{naziv:req.body.naziv}}).then(
        res.end("Tip obrisan"));
});

app.put('/v2/tip',function(req,res){
    var url = req.url;
    var ide = url.split("id=");

    db.tip.update({naziv:req.body.naziv},{where:{id:ide[1]}}).then(
        res.end("Azuriran tip"));
});


//za studenta
app.post('/v2/student',function(req,res){
    db.student.create({ime:req.body.ime , index:req.body.index}).then(
        res.end("Dodan student"));
});

app.get('/v2/student',function(req,res){
    res.setHeader('Content-Type', 'application/json');
    db.predmet.findAll({where:{ime:req.body.ime}}).then(function(rez){
        res.end(JSON.stringify(rez));
    });
});

app.delete('/v2/student',function(req,res){
    db.predmet.destroy({where:{naziv:req.body.naziv}}).then(
        res.end("Student obrisan"));
});

app.put('/v2/student',function(req,res){
    var url = req.url;
    var ide = url.split("id=");

    db.student.update({ime:req.body.ime,index:req.body.index},{where:{id:ide[1]}}).then(
        res.end("Azuriran student"));
});

app.put('/v2/student/:student_id/grupa/:grupa_id',function(req,res){
    var id_student = req.params.student_id;
    var id_grupe = req.params.grupa_id;

    db.student.findAll({where:{id:id_student}}).then(function(f){
        db.grupa.findAll({where:{id:id_grupe}}).then(function(f1){
            f1[0].addStudent(f[0]);
            console.log("Napravljena veza!");
        });
    });

});

app.listen(8080);
