const Sequelize = require("sequelize");
const sequelize = new Sequelize("bwt2066ST","root","root",{host:"localhost",dialect:"mysql"});
const db={};

db.Sequelize = Sequelize;  
db.sequelize = sequelize;

//import modela
db.aktivnost = require(__dirname + '/tabele/aktivnost.js')(sequelize,Sequelize.DataTypes);
db.dan = require(__dirname+'/tabele/dan.js')(sequelize,Sequelize.DataTypes);
db.grupa = require(__dirname+'/tabele/grupa.js')(sequelize,Sequelize.DataTypes);
db.predmet = require(__dirname + '/tabele/predmet.js')(sequelize,Sequelize.DataTypes);
db.student = require(__dirname+'/tabele/student.js')(sequelize,Sequelize.DataTypes);
db.tip = require(__dirname+'/tabele/tip.js')(sequelize,Sequelize.DataTypes);

//relacije
// VEZE
db.predmet.hasMany(db.grupa,{as:'grupniPredmet',foreignKey:'predmet_id',allowNull:false});

db.predmet.hasMany(db.aktivnost,{as:'aktivnosti',foreignKey:'predmet_id',allowNull:false});

db.grupa.hasMany(db.aktivnost,{as:'grupneAktivnost',foreignKey:'grupa_id',allowNull:true});

db.dan.hasMany(db.aktivnost,{as:'dnevneAktivnost',foreignKey:'dan_id',allowNull:false});

db.tip.hasMany(db.aktivnost,{as:'tipAktivnosti',foreignKey:'tip_id',allowNull:false});

db.grupeStudenata = db.student.belongsToMany(db.grupa,{as:'grupa',through:'grupa_studenta',foreignKey:'studentId'});
db.grupa.belongsToMany(db.student,{as:'student',through:'grupa_studenta',foreignKey:'grupaId'});



module.exports=db;